# Modul 133 - Arbeitsjournal Rogelio Barahona

## 27.08.2018
### Tätigkeiten 
Ich musste mich für ein Projekt entscheiden. Der Partner war mir noch nicht bekannt,
da die Projekte gelost wurden. Nach der Auslosung wusste ich, dass ich mit Fabio Stecher zusammen
arbeiten würde. Dies ist ein toller Partner.
Ich und Fabio arbeiteten uns in das Projekt ein und lasen aufmerksam die Dokumente auf dem GBC Drive durch.
Anschliessend wurden einige wichtige Punkte besprochen, zum Beispiel welche Git Plattform wir in Zukunft benutzen werden.

### Erfolge
- Es wurde entschieden, welche Git Plattform wir benutzen. Die Git Plattform heisst Bitbucket.

### Probleme
Bis jetzt sind keine Probleme aufgetaucht.

### Nächste Schritte
Der nächste Schritt wird sein, die ausgewählt Git Plattform für beide Projektbeteiligten zu konfigurieren.

### Persönliche Würdigung
Für mich war es ein angenehmer Projektstart. Da hatte ich schon schlimmere Projektanfänge.



## 03.09.2018
### Tätigkeiten 
Bitbucket wurde vollständig konfiguriert. Beide Projektbeteiligte konnten erfolgreich Dateien uploaden und downloaden.
Für die Bitbucket Plattform gibt es einige Zusatzmodule. Mit diesen werden wir uns in Zukunft auch noch beschäftigen müssen.
Zusätzlich haben wir für die Software "Sourcetree" heruntergeladen. Diese ermöglicht uns, Dateien von einem beliebigen PC aus,
auf unsere Bitbucket Plattform zu uploaden.
Fabio erstellte in dieser Lektion noch die Vorlage für die zukünftige Projektdokumentation.

### Erfolge
- Bitbucket wurde vollständig konfiguriert
- Die Software Sourcetree wurde auf unseren externen Festplatten installiert
- Die Dokumentationsvorlage mit einigen Inhaltspunkten wurde eingerichtet

### Probleme
Bis jetzt sind keine Probleme aufgetaucht.

### Nächste Schritte
Als nächstes werden die User Storys und Use Case Diagramme erstellt und in die Projektdokumentation eingefügt.
Die Datenbankablage wird im nächsten Schritt auch besprochen.

### Persönliche Würdigung
Da ich Bitbucket noch nicht kannte, musste ich mich einarbeiten. Dazu kam die Sourcetree Software die auch neu für mich war.
Ich fand das Dateiuploadsystem wirklich spannend und werde dies in Zukunft sicherlich mehrmals benutzen.



## 10.09.2018
### Tätigkeiten 
Kein Unterricht. Ich arbeitete in meinem Betrieb selbstständig an den User Storys. Dazu erstellte ich ein Word Dokument und habe es
mittels der Software Sourcetree auf unsere Bitbucket Plattform hochgeladen.

### Erfolge
- Die User Storys wurden sorgfältig erstellt.

### Probleme
Bis jetzt sind keine Probleme aufgetaucht.

### Nächste Schritte
In der nächsten Lektion werden Fabio und ich die User Storys gemeinsam anschauen und entscheiden, ob noch mehr User Storys nötig sind.
Nach der Besprechung sollen die User Storys in die Projektdokumentation eingefügt werden.

### Persönliche Würdigung
Die User Storys zu erstellen war sehr mühsam, da ich es zum ersten Mal selber durchführen musste. Man muss sehr viele Punkte beachten.
Zudem investierte ich sehr viel Zeit in die User Storys, da ständig etwas geändert werden musste. Hier habe ich eine neue Erfahrung gesammelt.



## 17.09.2018
### Tätigkeiten 
Fabio und ich besprachen gemeinsam die User Storys. Bis jetzt sind beide zufrieden. Die User Storys wurden in die Projektdokumentation eingefügt.
Die Projektdokumentation wurde von erweitert. Die Projektmanagement Methode wurde beschrieben und ein Namenskonzept wurde erstellt. Zudem wurde in
der Dokumentation erklärt, wie unser Backupsystem aussieht.

### Erfolge
- Die User Storys wurden in die Projektdokumentation eingefügt.
- Projektmanagement Methode und Backupsystem wurden erläutert

### Probleme
Bis jetzt sind keine Probleme aufgetaucht.

### Nächste Schritte
Als nächstes werden die Wireframes erstellt für die zukünftigen Seiten der Community Webseite.

### Persönliche Würdigung
Für mich war die Projektmanagementmethode "IPERKA" sozusagen neu, da ich immer mit HERMES gearbeitet habe (auch im Betrieb). Ich habe festgestellt,
dass die IPERKA Methode einfacher aufgebaut ist und für Schüler besser geeignet ist. Auch hier habe ich nun eine neue Erfahrung sammeln können.



## 24.09.2018 und 25.09.2018
### Tätigkeiten 
Ich habe mit den Wireframes angefangen. Dazu habe ich mir das Tool MockFlow heruntergeladen. Dies ist ein Tool, dass für die Erstellung von Wireframes
verwendet werden kann. In diesem Tool sind sehr viele nützliche Funktionen zu finden. Leider kosten Extra-Funktionen. Die erstellten Wireframes wurden
in die Projektdokumentation eingefügt und beschrieben. Zusätzlich erstellte ich an diesem Tag ein Backup von unseren Projektdaten auf meiner externen
GIBBIX Festplatte.

### Erfolge
- Wireframes wurden fertig erstellt
- Erfolgreiches Backup der Projektdateien auf eine externe Festplatte

### Probleme
Bis jetzt sind keine Probleme aufgetaucht.

### Nächste Schritte
Als nächstes werden wir die fehlenden Informationen in der Projektdokumentation nachtragen. Anschliessend wird mit der Realisierungsphase begonnen.
Die Realisierungsphase erfolgt nach den Wireframes, da unsere Wireframes sich sehr genau auf unsere zukünftige Webseite beziehen.

### Persönliche Würdigung
Auch hier habe ich zum ersten Mal die sogenannten Wireframes erstellt. Die Erstellung und Bearbeitung der Wireframes gefiel mir sehr. Ich konnte
dieses Erkenntnis auch für mein Projekt im Betrieb mitnehmen / übernehmen.



## 01.10.2018
### Tätigkeiten 
Ich habe die Problemdarstellung erstellt. Das heisst es wurden Meilensteine analysiert. Zudem habe ich Lösungen zu den Gefahren gesucht.
Dies habe ich ersichtlich in die Projektdokumentation implementiert. Zusätzlich erstellte ich eine Risikomatrix mit 16 Gefahrpunkten.
Die Meilensteinanalyse kontrollierte ich nochmal und bereitete alles vor um diese mit meinem Projektpartner Fabio Stecher anzuschauen.

### Erfolge
- Gefahranalyse / Meilensteinanalyse wurde erstellt, somit konnten Gefahren aufgelistet werden
-Risikomatrix wurde erstellt

### Probleme
Ein grosses Problem war der Zeitaufwand. Ich musste 3 Schullektionen für das Modul 133 investieren. Nun hatte ich weniger Zeit für das Modul 182.

### Nächste Schritte
Der nächste Schritt wird sein, die Gefahrenanalyse mit Fabio Stecher anzuschauen und möglicherweise weitere Punkte hinzuzufügen.

### Persönliche Würdigung
Ich erstellte die Gefahrenanalyse ohne grosse Schwierigkeiten. Es gefällt mir, Situationen strukturiert zu beobachten, zu analysieren und auszuwerten.



## 08.10.2018 bis 19.10.2018
### Tätigkeiten 
Nun sind zwei Wochen Schulferien. Nach der Auswertung der möglichen Meilensteine habe ich mit dem Frontend angefangen. Während der Arbeitszeit
programmierte ich zwischendurch. Nach diesen zwei Wochen bin ich mit dem Frontend sehr weit gekommen. Erfolge des Frontends werden im nächsten
Abschnitt aufgelistet.

### Erfolge
- Frontend: Startseite unserer Webseite ist nun fertig (index.html, style.css)
- Frontend: Loginseite ist nun fertig (login.html, style.css)
- Frontend: Registrationseite ist nun fertig (registrieren.html, style.css)

### Probleme
Bis jetzt konnte ich keine Probleme feststellen. Der Quellcode und die CSS-Datei mussten einige Male geändert werden wegen der Aufstellung, dies
zählt jedoch nicht als ein wirkliches Problem.

### Nächste Schritte
Als nächstes werde ich die Profilbearbeitungsseite einrichten und den News-Feed für die Benutzer. Dies erfordert viel Aufwand, da alles getestet
und protokolliert werden muss.

### Persönliche Würdigung
Ich bin froh, dass ich für die Erstellung des Frontends zuständig bin. Nach jeder kleinen Änderung im HTML oder CSS Quellcode sehe ich gerne
die Veränderung (F5 für Aktualisierung).



## 22.10.2018
### Tätigkeiten 
Nach Absprache mit Fabio Stecher waren die Startseite, die Loginseite und die Registrationsseite in Ordnung (vom Design her).
Nun testete ich selber die Seiten nochmals und erstellte ein Testprotokoll. Das Testergebnis ist in der Projektdokumentation vorhanden. Nach den
Tests fing ich mit der Profiländerungsseite an. Dies war ein grösserer Aufwand als ich gedacht habe. In den zwei Schullektionen konnte ich die
Profilbearbeitungsseite nicht fertig erstellen.

### Erfolge
- Testprotokoll der bisherigen Seiten erstellt und in die Dokumentation eingefügt
- Profilbearbeitungsseite anfangen zu erstellen

### Probleme
Ein Problemfaktor war, dass ich mich nicht für die Textbox Aufstellung entscheiden konnte. Ich änderte immer wieder die Aufstellung, so verging
viel Zeit und ich habe die Seite nicht fertig erstellen können.

### Nächste Schritte
Im Betrieb oder in der nächsten Informatiklektion werde ich die Profilbearbeitungsseite beenden. Zusätzlich werde ich mit der News-Feed Seite beginnen.

### Persönliche Würdigung
In schwierigen Situationen benötigt man sehr viel Geduld. Zum Beispiel konnte ich mich, wie erwähnt, für die Aufstellung / Design der Profilbearbeitungsseite
nicht entscheiden. Ein Erkenntnis war viel Geduld zu haben.



## 29.10.2018
### Tätigkeiten 
Während den zwei Schullektionen habe ich die News-Feed Seite erstellt. Zudem habe ich laufend die Dokumentation erweitert mit weiteren wichtigen Punkten
über das Frontend. Das Testprotokoll wird während dem Erstellen der News Feed Seite fortlaufend geführt. Es gab noch einige Anpassungen an den Sequenzendiagrammen.
Diese habe ich angepasst und nochmal korrigiert.

### Erfolge
- Frontend: News-Feed Seite erstellt für Benutzer
- Testprotokoll und Dokumentation fortlaufend geführt und angepasst

### Probleme
Bis jetzt sind keine grossen Probleme aufgetaucht.

### Nächste Schritte
Als nächstes werde ich die News-Feed Seite fertig designen. Anschliessend muss ich gewisse Schritte bzw. Punkte bezüglich der Backend Integration mit 
Fabio Stecher besprechen (Frontend / Backend).

### Persönliche Würdigung
Huete hatte ich keine wirklich grossen Erkenntnisse. Eigentlich waren meine heutigen Schritte klar definiert. Meine Ziele wurden in diesen zwei 
Schullektionen erreicht.



## 05.11.2018
### Tätigkeiten 
Fabio Stecher und ich haben in der ersten Schullektion weitere Schritte besprochen und wie die Intergration des Backend Produkts ins Frontend Produkt 
geschehen soll. Nach der Besprechung arbeitete ich an der News-Feed Seite weiter. Heute habe ich das Frontend der News-Feed Seite fertig gebracuht.
Aus meiner Sicht habe ich das Design ziemlich gut getroffen.

### Erfolge
- Pendenzenbesprechung erfolgreich geführt
- News-Feed Seite erfolgreich designed

### Probleme
Es tauchten keine Probleme auf. Wir sind im Zeitplan.

### Nächste Schritte
Der nächste Schritt wird sein, die Integration des Backend Produkts ins Frontend Produkt zu testen. Dazu müssen beide Projektmitglieder
anwesend sein. Es wird von beiden ein Testprotokoll geführt.

### Persönliche Würdigung
Ein Erkenntis war, an einer Projektbesprechung teilzunehmen. Dies musste ich zwar im Betrieb schon oft, jedoch ist dies mein eigenes Projekt.



## 12.11.2018
### Tätigkeiten 
Nach verschiedenen Absprachen und Diskussionen erklärte ich mich Fabio zu helfen. Ich habe ihm geholfen, das Backend Produkt zu programmieren. Als erstes programmierten
wir das Login und die ganze Registrierung für die Benutzer. Dies war nicht all zu schwer, da es im Internet und auf YouTube sehr viele Tutorials gegeben hat.

### Erfolge
- Frontend Login mit Backend Login "zusammengefügt"
- Funktionierende Loginseite
- Funktionierende Registrieungsseite
- Schutz gegen SQL Injektion

### Probleme
Es tauchten keine Probleme auf. Wir sind im Zeitplan.

### Nächste Schritte
Als nächsten Schritt werden wir den News Feed erstellen. Dies könnte länger als das Login dauern.

### Persönliche Würdigung
Zuerst wollte ich nicht am Backend teilnehmen, da ich praktisch keine Zeit habe. Nach einiger Zeit jedoch, bekam ich Motivation und Lust am programmieren.



## 19.11.2018
### Tätigkeiten 
Ich programmierte heute und am nächsten Tag im Betrieb den News Feed. Ich musste ziemlich viele Sachen beachten. Am Frontend musste das Design noch angepasst werden, da
nicht alle Symbole vorhanden waren. Nach zwei Tagen programmieren war der News Feed fertig und konnte von den Benutzern benutzt werden.

### Erfolge
- Funktionierendes News Feed
- Schutz gegen SQL Injektion

### Probleme
Frontend musste erneut angepasst werden.

### Nächste Schritte
Als nächsten Schritt werden wir eine Benutzersuche einrichten. Benutzer sollen andere Benutzer suchen können

### Persönliche Würdigung
Viele neue Befehle und Zeichen der Programmierung kennen gelernt. Für mich war die "PHP Session" oder "Benutzer Session" etwas neues.



## 26.11.2018
### Tätigkeiten 
In der Schule habe ich angefangen an der Benutzersuche zu programmieren. Als Programmiersprache habe ich AJAX integriert. Nun kann man durch AJAX eine Livebenutzersuche integrieren.

### Erfolge
- Funktionierende Benutzersuche mit AJAX und PHP
- Schutz für die Benutzer (in Quellcode kommentiert)

### Probleme
Keine Probleme.

### Nächste Schritte
In der nächsten Lektion werde ich die Einstellungen für Benutzer programmieren. Benutzer sollen ihre Informationen ändern können.

### Persönliche Würdigung
Neue Programmiersprache kennen gelernt (AJAX). Es war sehr viel Aufwand diese Sprache ein wenig zu meistern, jedoch nach einem Tag ist es mir gelungen.



## 03.12.2018
### Tätigkeiten 
Krankheit -> Abwesend

### Erfolge
- Keine neuen Erfolge

### Probleme
Nicht mehr im Zeitplan -> nächste Lektion wieder aufholen!

### Nächste Schritte
Einstellungen für Benutzer programmieren + eine Fotogalerie für die Benutzer.

### Persönliche Würdigung
Keine Persönliche Würdigung



## 10.12.2018
### Tätigkeiten 
In dieser Lektion musste ich ziemlich schnell vorankommen, da es die zweitletzte Lektion war um am Projekt zu arbeiten. Im Betrieb nehme ich mir auch noch Zeit um am Projekt weiter zu arbeiten.
Ich fing an, die eine Benutzerseite für die Einstellungen und Informationen zu erstellen. Dies programmierte ich im Betrieb fertig. Zusätzlich habe ich unsere Datenbank nochmals ein wenig angepasst, 
da nicht alle Beziehungen vorhanden waren (user -> mit fotos).

### Erfolge
- Funktionierende Benutzereinstellungsseite
- Funktionierende Datenbank mit Beziehungen

### Probleme
Es sind keine Probleme aufgetaucht.

### Nächste Schritte
Als nächstes werde ich ein Password Recovery programmieren, da Bneutzer ein Passwort vergessen könnten.



## 17.12.2018
### Tätigkeiten 
Krankheit -> Abwesend

### Erfolge
- Keine neuen Erfolge

### Probleme
Nicht mehr im Zeitplan -> nächste Lektion wieder aufholen!

### Nächste Schritte
In den Winterferien werde ich die Passwortzurücksetzung programmieren.

### Persönliche Würdigung
Keine Persönliche Würdigung



## 24.12.2018 - 06.01.2019
### Tätigkeiten 
Ich habe die Passwortzurücksetzung programmiert. Nun kann ein Benutzer auf "Passwort vergessen" und erhält per E-Mail ein neues generiertes Passwort.

### Erfolge
- Funktionierende Passwortzurücksetzung

### Probleme
Feed Item Bewertung funktioniert nicht!

### Nächste Schritte
Keine nächsten Schritte -> Produktpräsentation am 07.01.2019

### Persönliche Würdigung
Keine Persönliche Würdigung