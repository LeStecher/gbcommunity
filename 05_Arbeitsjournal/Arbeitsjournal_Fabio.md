# Modul 133 - Arbeitsjournal Fabio Stecher
## 27.08.2018
### Tätigkeiten
- Auswahl eines Projekts mit Zuteilung eines Mitschülers
- Administrative Dinge für das Projekt erledigt (Auswahl von VCS etc.) und Dokumente gelesen

### Erfolge
- Entscheidung für eine VCS (Version Control System). Die Entscheidung fiel auf BitBucket (Atlassian)

### Probleme
- Keine grösseren Probleme

### Nächste Schritte
- BitBucket für alle Projektbeteiligten einrichten
- Dateiablage für Dokumente vorbereiten
- Mit Dokumentation beginnen

### Persönlichewürdigung
Heute ging alles sehr gut. Ich und Rogelio sind, meiner Meinung nach, ein gutes Team. Die Aufgabe scheint sehr umfangreich.
Aber mit unserem Engagement können wir das schon bewältigen.

## 03.09.2018
### Tätigkeiten
- Einrichutng von BitBucket vorerst beendet
- Einerbeitung in die Software SourceTree (SourceTree ist eine grafische Benutzeroberfläche zur Bedienung der Versionskontrollsysteme Git und Mercurial)
- Erstellen des Gerüsts für die Dokumentation

### Erfolge
- BitBucket fertig konfiguriert
- Inbetriebnahme SourceTree
- Mit Dokumentation begonnen

### Probleme
- Handhabung mit SourceTree ist noch nicht ganz klar

### Nächste Schritte
- Erstellen von User Stories um anschliesssend die benötigten UML-Diagramme zu erstellen
- Dokumentation und Arbeitsjournal vervollständigen

### Persönlichewürdigung
Alles ging wieder ziemlich gut. Auch wenn die Handhabung von SourceTree nicht ganz klar ist, konnten wir die ersten Push- und Pull-Befehle ausführen und Dateien "commiten".
Es ist noch ein wenig chaotisch, da die Zeit knapp ist und die Aufgabenteilung nicht immer ganz klar ist. Dies wird aber sich noch verbessert.


## 10.09.2018
### Kein Unterricht aufgrund der SwissSkills


## 17.09.2018
### Tätigkeiten
- Besprechung der User Stories
- Erweiterung der Dokumentation
- Konfiguration einer Domäne (hostpoint.ch)
- Abklärung der Erstellung der Diagramme

### Erfolge
- Erweiterung der Dokumentation
- Inbetriebnahme der Domäne

### Probleme
- Handhabung und Möglichkeiten von HostPoint noch nicht ganz klar

### Nächste Schritte
- Erstellung der Use Case-, Activity- und Sequenzdiagramme
- Erweiterung der Dokumentation 

### Persönlichewürdigung
Heute war es chaotischer als sonst, da nicht allen klar war, wie Activity- und Sequenzdiagramme aussehen und wie man diese erstellen werden.
Ausserdem nahm die Inbetriebnahme der Domäne sehr viel Zeit in Anspruch und somit konnte ich heute auch nicht viel erledigen.

## 10.09.2018
Keine Arbeiten da die Zeit für das Modul 182 gebraucht wurde

## 15/16.10.2018 
### Tätigkeiten
- Überarbeitung der Überschriften in der Dokumentation (aufräumen)
- Mit den Activity-Diagrammen begonnen
- Planung der nächsten Aufgabenteilung

### Erfolge
- Schneller Fortschritt mit den Activity-Diagrammen
- Dokumentation konnte ein wenig aufgeräumt werden und auch klarer strukturiert werden

### Probleme
- Schwierigkeiten mit der Erstellung der Activity-Diagramme aus den Use-Case-Diagrammen

### Nächste Schritte
- Beschprechung aller bisher erstellten Diagramme
- Erstellen der Sequenzdiagramme
- Erstellen des ER-Diagramms für das Backend

### Persönlichewürdigung
Alle Tätigkeiten gingen gut. Ich konnte schnell das System der Activity-Diagramme verstehen und so die Diagramme für unsere User-Stories erstellen.
Jedoch fande ich es ziemlich schwer aus den Use-Case-Diagrammen den genauen Zweck der jeweligen User-Story herauszufinden.
Aber ich denke ich konnte trotzdem gute Diagramme zeichenen, auch wenn es das erste Mal ist, dass ich solche Diagramme erstelle.
Die Dokumentation konnte, meiner Meinung nach auch übersichtlicher gestaltet werden. 

## 05/06/08.11.2018
### Tätigkeiten
- Begonnen Funktionen in PHP zu schreiben (Gerüst)
- ER-Diagramm bearbeitet und besprochen
- Kleine Tutorials über PHP gelesen um einen Einstieg zu finden
- Nach Alternativen für PHP gesucht (AJAX)
- Erledigte sowie offene Punkte besprochen und Aufgaben verteilt

### Erfolge
- Viele der Funktionen die eventuell später gebraucht werden schon als Gerüst implementiert und kommentiert

### Probleme
- PHP Kenntnisse aufrufen und diese im Projekt anwenden
- Vollständigkeit des ER-Diagramm und die Assozationen dazu finden

### Nächste Schritte
- ER-Diagramm fertigstellen und besprechen sowie auf den Datenbankserver laden
- Weiter an Backend arbeiten und sich in PHP einarbeiten

### Persönlichewürdigung
An diesen Tag ging es eher langsam vorwärts, da ich persönlcih nicht viel mit PHP zu tun habe. Ausserdem sind offenbar viele weitere Sprachen (AJAX und JavaScript) nötig, mit welchen ich ebenfalls seit einem üK nicht mehr in Kontakt gekommen bin. Ich bin aber zuversichtlich, dass einige Funktionalitäten implementiert werden können. Ausserdem war es ebenfalls schwierig das ER-Diagramm zu zeichnen, da ich möglichst viele redundante Informationen vermeiden möchte. Dies ist mir aber, meiner Meinung nach gut gelungen. <br>
Ich bin aber Rogelio shr dankbar, dass er viele Aufgaben übernimmt und er mich auch bei Aufgaben unterstützt. 

## 12.11.2018
### Tätigkeiten
- Absprache der nächsten Schritte und Verteilung der nächsten Aufgaben
- Absprache mit anderen Gruppen, welche Methoden sie für die Implementierung des Backends
- Besprechung Vergleich Ist-Zustand und Bewertungskriterien

### Erfolge
- Grosse Absprache bezüglich offener Punkte und den Bewertungskriterien
- ER-Diagramm konnte vorerst abgeschlossen werden
- Konstruktive Gespräche mit anderen Gruppen

### Probleme
- Das Verständniss für die verschiedenen Sprachen und wie diese miteinander arbeiten fehlt noch
- Die Zeit für die Umsetzung dieses Projekts wird langsam knapp

### Nächste Schritte
- Weiter an Backend arbeiten und sich in die verschiedenen Sprachen einlesen und versuchen zu verstehen
- Nachdem Rogelio das ER-Diagramm mit einem Mitarbeiter besprochen hat, dieses in auf die Datenbank spielen
- Die Sequenzdiagramme fertigstellen

### Persönlichewürdigung
Wie schon in den Tagen zuvor hapert es noch immer mit der Implementierung des Backends. Jedoch motiviert mich, dass Rogelio schon sehr weit mit dem Frontend ist und dieses auch gut aussieht. Ich bin auch wieder dankbar, das Rogelio einige Arbeiten freiwillig übernimmt, obwohl er schon vieles gemacht hat. Er ist eine tolle Unterstützung in diesem Projekt. Zu dem war das Gespräch mit Elia, in welchem er mir versucht zu erklären, wie PHP, JavaScript und AJAX zusammenarbeiten, hilfreich und ich konnte einen kleinen Einblick gewinnen, wie dies umgesetzt werden könnte.

## 10.12.2018
### Tätigkeiten
- Kleine Verbesserungen an Code sowie an der Dokumentation vorgenommen
- Kurze Besprechung mit Rogelio über nächste Schritte und Aufgaben (per Telefon)

### Erfolge
- Diese kleinen Verbesserungen konnten schnell und ohne grössere Probleme behoben werden

### Probleme
- Keine Probleme aufgetreten

### Nächste Schritte
- Dokumentation fertig für Abgabe machen
- Dokumentation allenfalls noch mit fehlenden Dingen erweiteren

### Persönliche Würdigung
Die Änderungen waren nur klein und konnten auch schnell behoben oder der bestehende Zustand konnte einfach erweitert werden. Die kurze Besprechung mit Rogelio per Telefon war gut. Wir konnten in kurzer Zeit viele Dinge klären und auch noch die nächsten Aufgaben planen.

## 17.12.2018
### Tätigkeiten
- Code optimiert
- Suchleiste auf Seiten entfernt, auf denen es nicht gebraucht wird, Seite für Profil eines Freundes begonnen zu erstellen

### Erfolge
- Code konnte ein wenig optimiert werden

### Probleme
- Schwierigkeiten bei der Implementation der Seite für das Profil eines Freundes, da der Ablauf des Codes (als Vorlage) nicht klar ist

### Nächste Schritte
- Dokumentation fertigstellen

### Persönliche Würdigung
Heute war nicht so ein guter Tag, da ich sehr grosse Schwierigkeiten hatt, dies Seite für das Profil eines Freundes zu erstellen und auch die Optimierung des Code war nicht immer so einfach, da in vielen Dateien viele Zeilenumbrüche verwendet wurden und auch die Einrückung der Codezeilen nicht immmer stimmte.

## 06.01.2019
### Tätigkeiten
- Testfälle definiert und durchgeführt
- Concept Map fertiggestellt
- Letzte Diagramme in Dokumentation eingefügt
- Dokumentation fertig gestellt

### Erfolge
- Testfälle fertig durchgeführt
- Dokumentation fertiggstellt

### Probleme
- Erst jetzt bemerkt, dass nicht alle Diagrammarten vollständig sind

### Nächste Schritte
- Abgabe der Arbeit
- Für eine Gute Note beten

### Persönliche Würdigung
Heute war ein guter Tag. Auch wenn die Dokumentation fünf vor 12 fertiggestellt wurde und nicht alles perfekt ist.
Alle Testfälle konnten durchgeführt werden und noch restliche Dinge fertiggestellt werden. Im nachhinein sind mir einige Dinge aufgefallen, die man hätte besser machen können, aber man sagt nicht umsonst: "Nachher ist man immer schläuer :-)". Aber übers ganze Projekt bin ioch froh, dass ich Rogelio als Partner hatte, da er mich super untersützt hat und auch einige meiner Aufgaben übernommen hat.