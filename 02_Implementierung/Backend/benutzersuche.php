<?php

session_start();

/* if(!isset($_SESSION['username'])) {
   die("Bitte erst einloggen"); //Mit die beenden wir den weiteren Scriptablauf   
} */

//$username = $_SESSION['username'];

//echo "$username";
?>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>GBCommunity Benutzersuche</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
	
	
	
	<body>
	
	<!-- Button um zurück zum Newsfeed zu gelangen -->
	<button type="submit" style="background-color: #B2B0B0;
		border: none;
		color: white;
		padding: 15px 32px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		margin-left:15px; 
		margin-top:15px; 
		border-radius:4px"><a href="newsfeed.php" style="color:white; text-decoration:none;">Zurück</a></button>
	<!-- ----------------------------------------------------------------- -->
	
		<div class="container">
			<br />
			<h2 align="center">GBCommunity Benutzersuche</h2><br />
			<div class="form-group">
				<div class="input-group">
					<span class="input-group-addon">Suche</span>
					<input type="text" name="search_text" id="search_text" placeholder="Benutzerdatenbank durchsuchen..." class="form-control" />
				</div>
			</div>
			<br />
			<div id="result"></div>
		</div>
		<div style="clear:both"></div>
		<br />
		
		<br />
		<br />
		<br />
	</body>
</html>

<!-- Java Funktion für die Anzeige der Benutzersuche, ist verknüpft auf php/fetch.php-->
<script>
$(document).ready(function(){
	load_data();
	function load_data(query)
	{
		$.ajax({
			url:"php/fetch.php",
			method:"post",
			data:{query:query},
			success:function(data)
			{
				$('#result').html(data);
			}
		});
	}
	
	$('#search_text').keyup(function(){
		var search = $(this).val();
		if(search != '')
		{
			load_data(search);
		}
		else
		{
			load_data();			
		}
	});
});
</script>




