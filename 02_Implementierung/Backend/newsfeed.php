<!-- ============================== HEADER ==============================

# Title:                newsfeed.php		

# Beschreibung:			login

# Autor:				Rogelio Barahona

# Datum:				05.11.2018

# Version:				1.0

# Verwendung			Anmeldung

--------------------------------------------------------------------------

Alle Rechte vorbehalten Gewerbliche Berufsschule Chur, 2018

==========================================================================-->



<?php

session_start();

if(!isset($_SESSION['username'])) {

   die("Bitte erst einloggen"); //Mit die beenden wir den weiteren Scriptablauf   

}



$username = $_SESSION['username'];



?>



<?php



$db = mysqli_connect("stecherf.mysql.db.hostpoint.ch", "stecherf_barrog", "vukXeD4V", "stecherf_gbcommunity");



$msg = "";



	if (isset($_POST['submit_post'])) {

		



	// Get image name

  	$image = $_FILES['feed_image']['name'];

	

	$joined_date = date_create()->format('Y-m-d H:i:s');

	

  	// Get text

  	$image_text = mysqli_real_escape_string($db, $_POST['image_text']);



  	// image file directory

  	$target = "pictures/".basename($image);



  	$sql = "INSERT INTO feed_item (feed_description, feed_image, timestamp, user_ID) VALUES ('$image_text', '$image', '$joined_date', (SELECT user_ID FROM user WHERE username='$username'))";

	

  	// execute query

  	//mysqli_query($db, $sql);

	$ce = mysqli_prepare($db, $sql);

	$execute = mysqli_stmt_execute($ce);



  	if (move_uploaded_file($_FILES['feed_image']['tmp_name'], $target)) {

  		$msg = "Image uploaded successfully";

		header('Refresh: 0 ; url=newsfeed.php');

  	}else{

  		$msg = "Failed to upload image";

  	}

  }

  

  $result = mysqli_query($db, "SELECT t1.feed_item_ID, t1.feed_image, t1.feed_description, t1.timestamp, t1.likes, t1.user_ID, t2.image, t2.username FROM feed_item AS t1, user AS t2 WHERE t1.user_ID = t2.user_ID ORDER BY t1.`feed_item_ID` DESC");	//Posts werden in Newsfeed angezeigt -> ORDER BY gleich neuster Post zu oberst!

  

?>





<!-- "Get" PHP Abschnitt für Benutzerinfos -->

<?php



$query_user = "SELECT first_name, last_name, username FROM user WHERE username = '".$username."' LIMIT 1";

	$result_user = mysqli_query($db, $query_user);

	

	while($row_user = mysqli_fetch_array($result_user))

	{

		$first_name = $row_user[0];

		$last_name = $row_user[1];

	}



?>





<!doctype html>

<html lang="en">

  <head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> <!-- Navbar -->

	<link href="https://www.maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> <!-- Footer -->

	

	<!-- Website Font style -->

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

			

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

	

	<link href="css/newsfeed.css" rel="stylesheet">

		

	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">

<!--===============================================================================================-->	

	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">

<!--===============================================================================================-->	

	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">

	

	

    <title>GBCommunity Newsfeed</title>

  </head>

  

 <!-- Navbar -->

    <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">

  <a class="navbar-brand" href="index.html">GBCommunity</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

    <span class="navbar-toggler-icon"></span>

  </button>



  <div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">

	  <li class="nav-item active">

        <a class="nav-link" href="newsfeed.php">

          <i class="fa fa-paper-plane"></i>

          News

          <span class="sr-only">(current)</span>

          </a>

      </li> 

		<li class="nav-item">

        <a class="nav-link disabled" href="meinprofil.php">

          <i class="fa fa-user">

            <span class="badge badge-danger"></span>

          </i>

          Profil

        </a>

      </li>

	  <li class="nav-item">

        <a class="nav-link disabled" href="profil.php">

          <i class="fa fa-gear">

            <span class="badge badge-danger"></span>

          </i>

          Einstellungen

        </a>

      </li>

	  <li class="nav-item">

        <a class="nav-link disabled" href="meinprofil.php">

		<span class="badge badge-danger"></span>

          Eingeloggt als: <?php echo "$first_name "."$last_name, "."$username"; ?> 

        </a>

      </li>

    </ul>
	<form class="form-inline my-2 my-lg-0">

      <!-- <input class="form-control mr-sm-2" type="text" placeholder="durchsuchen..." style="background-color:transparent; border-color:white; color:white;" aria-label="Search"> -->

	  <button class="fa fa-search" type="submit" style="color:white; background-color:transparent; border:transparent;"><div style="font-family:Segoe UI; font-weight:400;"><a href="benutzersuche.php" style="color:white; text-decoration:none;">Benutzersuche</a></div></button>

    </form>
	
	<form class="form-inline my-2 my-lg-0">

		<label class="form-control mr-sm-2" style="background-color:transparent; border-color:transparent; color:white; text-decoration:none;"><a href="index.html">Abmelden</a></label> <!-- Session wird gelöscht und der Benutzer wird auf Startseite weitergeleitet! SICHERHEITSASPEKT---->

		

	</form>

	

  </div>

</nav>

<!-- --------------------------------------------->



</br>



<body>



<div class="tweetEntry-tweetHolder">

<h1 class="tweetEntry" style="margin-bottom:0px; text-align:center;">Beiträge von Freunden</h1>



<div class="row">

<div class="col-md-12">

<form method="POST" action="newsfeed.php" enctype="multipart/form-data">

  	<div class="form-group row">

	<input type="hidden" name="size" value="1000000">

	</div>

  	<div class="form-group row">

	<div class="offset-4 col-8">

  	  <input type="file" name="feed_image">

	  </div>

  	</div>

  	<div class="form-group row">

	<div class="offset-4 col-8">

      <textarea 

      	id="text" 

      	cols="60" 

      	rows="3" 

      	name="image_text" 

      	placeholder="Bildbeschreibung oder Tags.." style="margin-left:-140px;"/></textarea>

		</div>

  	</div>

  	<div class="form-group row">

	<div class="offset-4 col-8">

  		<button type="submit" name="submit_post" style="margin-left:20px;">Bild posten</button>

		</div>

  	</div>

  </form>

  </div>

 </div>





<div class="tweetEntry">



  <?php

  	echo "<p>_____________________________________________________________________________________</p>";

	

    while ($row = mysqli_fetch_array($result)) //hier noch div html erstellen für angemessenes Design!

	{	



		echo "<div class='tweetEntry-content'>";

			echo "<a class='tweetEntry-account-group' href='freundesprofil.php'>"; //hier kommt link zum Benutzerprofil!

			echo "<img class='tweetEntry-avatar' src='pictures/".$row['image']."' >"; //hier kommt Profilbild von Benutzer alt: src='http://placekitten.com/200/200'

				echo "<strong class='tweetEntry-fullname'>";

					echo "<p>".$row['username']."</p>"; //Benutzername des Benutzers

				echo "</strong>";

			echo "<span class='tweetEntry-timestamp'>Gepostet am: ".$row['timestamp']."</span>";	

			echo "</a>";

			echo "<div class='tweetEntry-text-container'>";

				echo "<p>Beschreibung: ".$row['feed_description']."</p>";

			echo "</div>";

		echo "</div>";

		

		echo "<div class='optionalMedia'>";

			echo "<img class='optionalMedia-img' src='pictures/".$row['feed_image']."' >";

      	echo "</div>";

		

		echo "";

			echo "<div class='tweetEntry-action-list' style='line-height:24px;color: #b1bbc3;'>";

			echo "<i class='fa fa-heart' style='width:80px'></i>"; //hier kann man liken -> + Likezahlen wird daneben eingegeben

		echo "</div>";

		echo "<br />";

		echo "<p>_____________________________________________________________________________________</p>";

		

    }

  ?>

  

</div>  

</div>

	

</body>



<!-- Footer -->

<footer class="container-fluid text-center bg-lightgray">



        <div class="copyrights" style="margin-top:25px;">

            <p>GBCommunity © 2019, Alle Rechte vorbehalten

                <br>

                <span>Web Design By: Rogelio Barahona & Fabio Stecher</span></p>

            <p><a href="https://www.instagram.com/medicine.doc/?hl=de" target="_blank">Kontakt</a></p>

        </div>

</footer>

<!-- --------------------------------------------------->

</html>