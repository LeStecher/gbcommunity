<?php
    use PHPMailer\PHPMailer\PHPMailer;
    require_once "functions.php";

    if (isset($_POST['email'])) {
        $conn = new mysqli('stecherf.mysql.db.hostpoint.ch', 'stecherf_barrog', 'vukXeD4V', 'stecherf_gbcommunity');

		//		$db = mysqli_connect('stecherf.mysql.db.hostpoint.ch', 'stecherf_barrog', 'vukXeD4V', 'stecherf_gbcommunity'); 
        
		$email = $conn->real_escape_string($_POST['email']);

        $sql = $conn->query("SELECT user_ID FROM user WHERE email='$email'");
        if ($sql->num_rows > 0) {

            $token = generateNewString();

	        $conn->query("UPDATE user SET token='$token', 
                      tokenExpire=DATE_ADD(NOW(), INTERVAL 5 MINUTE)
                      WHERE email='$email'
            ");

	        require_once "PHPMailer/PHPMailer.php";
	        require_once "PHPMailer/Exception.php";

	        $mail = new PHPMailer();
	        $mail->addAddress($email);
	        $mail->setFrom("gbcommunity.adm@gmail.com", "GBCommunity Admin");
	        $mail->Subject = "Passwort zurücksetzen";
	        $mail->isHTML(true);
	        $mail->Body = "
	            Yoooo,<br><br>
	            
	            Hier erhalten Sie ein neues Passwort:<br>
	            <a href='
	            http://stecherf.myhostpoint.ch/resetPassword.php?email=$email&token=$token
	            '>http://stecherf.myhostpoint.ch/resetPassword.php?email=$email&token=$token</a><br><br>
	            
	            Freundliche Grüsse,<br>
	            GBCommunity Administrator
	        ";

	        if ($mail->send())
    	        exit(json_encode(array("status" => 1, "msg" => 'E-Mail gesendet, bitte Posteingang überprüfen.')));
    	    else
    	        exit(json_encode(array("status" => 0, "msg" => 'Etwas ist schief gelaufen! Bitte wiederholen!')));
        } else
            exit(json_encode(array("status" => 0, "msg" => 'E-Mail Adresse im System nicht vorhanden. Bitte nochmal versuchen.')));
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forgot Password System</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container" style="margin-top: 100px;">
        <div class="row justify-content-center">
            <div class="col-md-6 col-md-offset-3" align="center">
                <img src="img/gbcommunity.png"><br><br>
                <input class="form-control" id="email" placeholder="Your Email Address"><br>
                <input type="button" class="btn btn-primary" value="Passwort zurücksetzen">
                <br><br>
                <p id="response"></p>
            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        var email = $("#email");

        $(document).ready(function () {
            $('.btn-primary').on('click', function () {
                if (email.val() != "") {
                    email.css('border', '1px solid green');

                    $.ajax({
                       url: 'forgotPassword.php',
                       method: 'POST',
                       dataType: 'json',
                       data: {
                           email: email.val()
                       }, success: function (response) {
                            if (!response.success)
                                $("#response").html(response.msg).css('color', "red");
                            else
                                $("#response").html(response.msg).css('color', "green");
                        }
                    });
                } else
                    email.css('border', '1px solid red');
            });
        });
    </script>
</body>
</html>
