<!-- ============================== HEADER ==============================

# Title:                freundesprofil.php		

# Beschreibung:			Profil eines Freundes anzeigen

# Autor:				Fabio Stecher

# Datum:				21.12.2018

# Version:				1.0

# Verwendung			Profil eines Freundes anzeigen

--------------------------------------------------------------------------

Alle Rechte vorbehalten Gewerbliche Berufsschule Chur, 2018

==========================================================================-->
<?php
session_start();

$db = mysqli_connect('stecherf.mysql.db.hostpoint.ch', 'stecherf_barrog', 'vukXeD4V', 'stecherf_gbcommunity');

if(!isset($_SESSION['username'])) {

    die("Bitte erst einloggen"); //Mit die beenden wir den weiteren Scriptablauf   
 
}

$username = $_SESSION['username'];

$username = mysqli_real_escape_string($db, $username);

?>

<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->

        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> <!-- Navbar -->

        <link href="https://www.maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> <!-- Footer -->

        <link href="css/meinprofil.css" rel="stylesheet"> <!-- Diese CSS Datei enthält weitere Design Punkte die notwendig sind für die Aufstellung -->
        <!-- Website Font style -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">

        <!--===============================================================================================-->

        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">

        <!--===============================================================================================-->

        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> 

        <!--===============================================================================================-->

        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">

        <!--===============================================================================================-->	

        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">

        <!--===============================================================================================-->

        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">

        <!--===============================================================================================-->  

        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">

        <!--===============================================================================================-->	

        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        
        <title>Profil von [username]</title>
    </head>

    <!-- Navbar -->
    <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.html">GBCommunity</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
	            <li class="nav-item">
                    <a class="nav-link disabled" href="newsfeed.php">
                        <i class="fa fa-paper-plane">
                            <span class="badge badge-danger"></span>
                        </i>
                         News
                    </a>
                </li> 
	            <li class="nav-item">
                    <a class="nav-link" href="meinprofil.php">
                        <i class="fa fa-user"></i>
                        Profil
                    </a>
                </li>   
	            <li class="nav-item active">
                    <a class="nav-link" href="profil.php">
                        <i class="fa fa-gear"></i>
                        Einstellungen
                        <span class="sr-only">(current)</span>
                    </a>
                </li> 
            </ul>
	        <form class="form-inline my-2 my-lg-0">
		        <label class="form-control mr-sm-2" style="background-color:transparent; border-color:transparent; color:white; text-decoration:none;"><a href="index.html">Abmelden</a></label> <!-- Session wird gelöscht und der Benutzer wird auf Startseite weitergeleitet! SICHERHEITSASPEKT -->
	        </form>
        </div>
    </nav>
    <!-- ----------------------------------------------------------------------- -->
    </br>
    <div class="container emp-profile">
        <form method="post">
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
						<?php
						   $con = mysqli_connect("stecherf.mysql.db.hostpoint.ch","stecherf_barrog","vukXeD4V","stecherf_gbcommunity");
			                $q = mysqli_query($con,"SELECT * FROM user WHERE username='$username'");
                            while($row = mysqli_fetch_assoc($q)) 
                            {
                                if($row['image'] == ""){
					                echo "<img width='100px' height='100px' src='pictures/default.jpg' alt='Default Profile Pic'>";
                                } 
                                else {
					                echo "<div class='profile-img'><img width='100px' height='100px' src='pictures/".$row['image']."' alt='Profile Pic'></div>";

				                }
			                }
						?>
                        <div>
							<div class="form-group">
					        </div>
					    </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="profile-head">
                        <h5>
                            <?php echo "$first_name "."$last_name"; ?><!-- hier wird Name der Person stehen (Vor und Nachname) -->
                        </h5>
                        <h6>
                            <?php echo "$job"; ?> <!-- hier wird der Beruf stehen -->
                        </h6>
						</br>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Profil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Über mich</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Benutzername</label> 
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo "$username"; ?></p> <!-- Benutzername wird hier angezeigt -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo "$first_name "."$last_name"; ?></p> <!-- Vorname und Nachname wird hier angezeigt -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>E-Mail</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo "$email"; ?></p> <!-- E-Mail Adresse des Benutzers -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Telefonnummer</label> 
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo "$telnumber"; ?></p> <!-- Telefonnummer des Benutzers -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Beruf</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo "$job"; ?></p> <!-- hier wird Beruf angegeben (nochmal?) -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">                                       
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Status</label>
                                    <br/>
                                    <p><?php echo "$aboutme"; ?></p> <!-- Status wird hier angezeigt -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>           
    </div>

    <!-- JavaScript Dateien -->
    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->

	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>

    <!--===============================================================================================-->

	<script src="vendor/animsition/js/animsition.min.js"></script>

    <!--===============================================================================================-->

	<script src="vendor/bootstrap/js/popper.js"></script>

	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!--===============================================================================================-->

	<script src="vendor/select2/select2.min.js"></script>

    <!--===============================================================================================-->

	<script src="vendor/daterangepicker/moment.min.js"></script>

	<script src="vendor/daterangepicker/daterangepicker.js"></script>

    <!--===============================================================================================-->

	<script src="vendor/countdowntime/countdowntime.js"></script>

    <!--===============================================================================================-->

    <script src="js/main.js"></script>
    
    <!-- Footer -->

    <footer class="container-fluid text-center bg-lightgray">
        <div class="copyrights" style="margin-top:25px;">
            <p>GBCommunity © 2018, Alle Rechte vorbehalten
            <br>
            <span>Web Design By: Rogelio Barahona & Fabio Stecher</span></p>
            <p><a href="https://www.google.ch" target="_blank">Kontakt</a></p>
        </div>
    </footer>
    <!-- --------------------------------------------------->
</html>