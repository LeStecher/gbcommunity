<?php

session_start();


// initializing variables
$username = "";
$email    = "";
$errors = array();

// connect to the database
$db = mysqli_connect('stecherf.mysql.db.hostpoint.ch', 'stecherf_barrog', 'vukXeD4V', 'stecherf_gbcommunity');

// REGISTER USER
if (isset($_POST['reg_user'])) { //reg_user ist Name von Registrationsbutton
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);
  $first_name = mysqli_real_escape_string($db, $_POST['first_name']);
  $last_name = mysqli_real_escape_string($db, $_POST['last_name']);
  $telnumber = mysqli_real_escape_string($db, $_POST['telnumber']);
  
  $bdaydate = mysqli_real_escape_string($db, $_POST['bdaydate']);
  
  $joined_date = date_create()->format('Y-m-d H:i:s');
  
  $gender = mysqli_real_escape_string($db, $_POST['gender']);
  $city = mysqli_real_escape_string($db, $_POST['city']);
  $country = mysqli_real_escape_string($db, $_POST['country']);

  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Benutzername wird benötigt"); }
  if (empty($email)) { array_push($errors, "E-Mail wird benötigt"); }
  if (empty($first_name)) { array_push($errors, "Vorname wird benötigt"); }
  if (empty($last_name)) { array_push($errors, "Nachname wird benötigt"); }
  if (empty($telnumber)) { array_push($errors, "Telefonnummer wird benötigt"); }
  if (empty($bdaydate)) { array_push($errors, "Geburtsdatum wird benötigt"); }
  if (empty($gender)) { array_push($errors, "Geschlecht wird benötigt"); }
  if (empty($city)) { array_push($errors, "Wohnort wird benötigt"); }
  if (empty($country)) { array_push($errors, "Land wird benötigt"); }
  if (empty($password_1)) { array_push($errors, "Passwort wird benötigt"); }
  if ($password_1 != $password_2) {
	array_push($errors, "Passwörter stimmen nicht überein");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM user WHERE username='$username' OR email='$email' OR telnumber='$telnumber' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['email'] === $email) {
      array_push($errors, "email already exists");
    }
	
	if ($user['telnumber'] === $telnumber) {
      array_push($errors, "Telefonnummer already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = md5($password_1);//encrypt the password before saving in the database

  	$query = "INSERT INTO user (username, email, password, first_name, last_name, telnumber, bdaydate, joined_date, gender, city, country) 
  			  VALUES('$username', '$email', '$password', '$first_name', '$last_name', '$telnumber', '$bdaydate', '$joined_date', '$gender', '$city', '$country')";
  	mysqli_query($db, $query);
  	$_SESSION['username'] = $username;
	$_SESSION['first_name'] = $first_name;
  	$_SESSION['success'] = "Du bist nun eingeloggt";
  	header('location: login.php'); //vorher: index.php
  }
}

//.......... 




?>