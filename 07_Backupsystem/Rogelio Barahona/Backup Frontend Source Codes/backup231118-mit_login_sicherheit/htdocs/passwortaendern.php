<!-- ============================== HEADER ==============================
# Title:                passwortaendern.html			
# Beschreibung:			Passwortänderung
# Autor:				Rogelio Barahona
# Datum:				05.11.2018
# Version:				1.0
# Verwendung			Passwort ändern
--------------------------------------------------------------------------
Alle Rechte vorbehalten Gewerbliche Berufsschule Chur, 2018
==========================================================================-->

<?php
    use PHPMailer\PHPMailer;
    require_once "php/functions.php";

    if (isset($_POST['submit'])) {
        $conn = new mysqli('localhost', 'root', '', 'gbcommunity');

        $email = $conn->real_escape_string($_POST['email']);

        $sql = $conn->query("SELECT user_ID FROM user WHERE email='$email'");
        if ($sql->num_rows > 0) {

            $token = generateNewString();

	        $conn->query("UPDATE user SET token='$token', 
                      tokenExpire=DATE_ADD(NOW(), INTERVAL 5 MINUTE)
                      WHERE email='$email'
            ");

	        require_once "PHPMailer/PHPMailer.php";
	        require_once "PHPMailer/Exception.php";

	        $mail = new PHPMailer();
	        $mail->addAddress($email);
	        $mail->setFrom("", "");
	        $mail->Subject = "Reset Password";
	        $mail->isHTML(true);
	        $mail->Body = "
	            Hi,<br><br>
	            
	            In order to reset your password, please click on the link below:<br>
	            <a href='
	            http://domain.com/resetPassword.php?email=$email&token=$token
	            '>http://domain.com/resetPassword.php?email=$email&token=$token</a><br><br>
	            
	            Kind Regards,<br>
	            My Name test
	        ";

	        if ($mail->send())
    	        exit(json_encode(array("status" => 1, "msg" => 'Please Check Your Email Inbox!')));
    	    else
    	        exit(json_encode(array("status" => 0, "msg" => 'Something Wrong Just Happened! Please try again!')));
        } else
            exit(json_encode(array("status" => 0, "msg" => 'Please Check Your Inputs!')));
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> <!-- Navbar -->
	<link href="https://www.maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> <!-- Footer -->
	
	<!-- <link href="css/style.css" rel="stylesheet"> -->
	
	<!-- Website Font style -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
			
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
		
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">	
	
	
	
    <title>GBCommunity Passwort ändern</title>
  </head>
  
  <!-- Navbar -->
    <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="index.html">GBCommunity</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link disabled" href="registrieren.html">
          <i class="fa fa-home">
            <span class="badge badge-danger"></span>
          </i>
          Startseite
        </a>
      </li>
	  <li class="nav-item">
        <a class="nav-link disabled" href="newsfeed.html">
          <i class="fa fa-paper-plane">
            <span class="badge badge-danger"></span>
          </i>
          News
        </a>
      </li> 
	  <li class="nav-item">
        <a class="nav-link" href="meinprofil.html">
          <i class="fa fa-user"></i>
          Profil
          </a>
      </li>   
	  <li class="nav-item active">
        <a class="nav-link" href="profil.html">
          <i class="fa fa-gear"></i>
          Einstellungen
          <span class="sr-only">(current)</span>
          </a>
      </li>  
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" placeholder="durchsuchen..." style="background-color:transparent; border-color:white; color:white;" aria-label="Search">
      <button class="fa fa-search" type="submit" style="color:white; background-color:transparent; border:transparent;"></button>
    </form>
  </div>
</nav>
<!-- --------------------------------------------->

</br>

<div class="container">
	<div class="row">
		<div class="col-md-3 ">
		     <div class="list-group ">
			  <a href="profilbild.html" class="list-group-item list-group-item-action">Profilbild</a>
              <a href="profil.html" class="list-group-item list-group-item-action">Profil bearbeiten</a>
              <a href="passwortaendern.html" class="list-group-item list-group-item-action active">Passwort ändern</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              <a href="#" class="list-group-item list-group-item-action">Test</a>
              
              
            </div> 
		</div>
		
		<div class="col-md-9">
		    <div class="card">
		        <div class="card-body">
		            <div class="row">
		                <div class="col-md-12">
		                    <h4>Mein Profil</h4> <!-- hier könnte Name von Benutzer stehen -->
		                    <hr>
		                </div>
		            </div>
		            <div class="row">
		                <div class="col-md-12">
		                    <form>
                              <div class="form-group row">
                                <label for="altpasswort" class="col-4 col-form-label">altes Passwort</label> 
                                <div class="col-8">
                                  <input id="altpasswort" name="altpasswort" placeholder="" class="form-control here" required="required" type="password"> <!-- hier altes Password eingeben! -->
                                </div>
                              </div>
							  <div class="form-group row">
                                <label for="neupasswort" class="col-4 col-form-label">Neues Passwort</label> 
                                <div class="col-8">
                                  <input id="neupasswort" name="neupasswort" placeholder="" class="form-control here" required="required" type="password"> <!-- neues Passwort eingeben -->
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="neupasswortwiederholen" class="col-4 col-form-label">Neues Passwort wiederholen</label> 
                                <div class="col-8">
                                  <input id="neupasswortwiederholen" name="neupasswortwiederholen" placeholder="" class="form-control here" type="password"> <!-- neues Passwort erneut eingeben -->
                                </div>
                              </div>
							  
                              <div class="form-group row">
                                <div class="offset-4 col-8">
                                  <button name="submit" type="submit" class="btn btn-primary">Passwort ändern</button>
                                </div>
                              </div>
                            </form>
		                </div>
		            </div>
		            
		        </div>
		    </div>
		</div>
		
		
	</div>
</div>

<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        var email = $("#email");

        $(document).ready(function () {
            $('.btn-primary').on('click', function () {
                if (email.val() != "") {
                    email.css('border', '1px solid green');

                    $.ajax({
                       url: 'passwortaendern.php',
                       method: 'POST',
                       dataType: 'json',
                       data: {
                           email: email.val()
                       }, success: function (response) {
                            if (!response.success)
                                $("#response").html(response.msg).css('color', "red");
                            else
                                $("#response").html(response.msg).css('color', "green");
                        }
                    });
                } else
                    email.css('border', '1px solid red');
            });
        });
    </script>

<!-- JavaScript Dateien -->
  <div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
  
<!-- Footer -->
<footer class="container-fluid text-center bg-lightgray">

        <div class="copyrights" style="margin-top:25px;">
            <p>GBCommunity © 2018, Alle Rechte vorbehalten
                <br>
                <span>Web Design By: Rogelio Barahona & Fabio Stecher</span></p>
            <p><a href="https://www.google.ch" target="_blank">Kontakt</a></p>
        </div>
</footer>
<!-- --------------------------------------------------->
</html>