<!-- ============================== HEADER ==============================
# Title:                registrieren.php	
# Beschreibung:			Registrationsseite
# Autor:				Rogelio Barahona
# Datum:				05.11.2018
# Version:				1.0
# Verwendung			Benutzerregistration
--------------------------------------------------------------------------
Alle Rechte vorbehalten Gewerbliche Berufsschule Chur, 2018
==========================================================================-->

<?php include('php/server.php') ?> 

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> <!-- Navbar -->
	<link href="https://www.maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> <!-- Footer -->
	<!-- Website Font style -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
			
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
		
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">	
		
	
    <title>GBCommunity Registrierung</title>
  </head>
  
  <!-- Navbar -->
    <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="index.html">GBCommunity</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="fa fa-home">
            <span class="badge badge-danger"></span>
          </i>
          Startseite
        </a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="login.php">
          <i class="fas fa-angle-double-right">
            <span class="badge badge-danger"></span>
          </i>
          Anmelden
        </a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="registrieren.php">
          <i class="fas fa-angle-double-right"></i>
          Registrieren
          <span class="sr-only">(current)</span>
          </a>
      </li>
	  
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="text" oninput="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')" placeholder="durchsuchen..." style="background-color:transparent; border-color:white; color:white;" aria-label="Search">
      <button class="fa fa-search" type="submit" style="color:white; background-color:transparent; border:transparent;"></button>
    </form>
  </div>
</nav>
<!-- --------------------------------------------->


<div class="container">
</br>

<div class="row justify-content-center">
<div class="col-md-6">
<div class="card">
<header class="card-header">
	<!-- <a href="" class="float-right btn btn-outline-primary mt-1">Anmelden</a> -->
	<h4 class="card-title mt-2" style="font-family: Segoe UI;">Registrierung</h4>
</header>
<article class="card-body">
<form method="post" action="registrieren.php">
<?php include('php/errors.php'); ?>
	<div class="form-row">
		<div class="col form-group">
			<label>Vorname</label>   
		  	<input type="text" class="form-control" placeholder="" oninput="this.value = this.value.replace(/[^a-zA-Z]/, '')" name="first_name">
		</div> <!-- form-group end.// -->
		<div class="col form-group">
			<label>Nachname</label>
		  	<input type="text" class="form-control" placeholder="" oninput="this.value = this.value.replace(/[^a-z]/, '')" name="last_name">
		</div> <!-- form-group end.// -->
	</div> <!-- form-row end.// -->
	<div class="form-group">
		<label>Benutzername</label>
		<input type="text" class="form-control" placeholder="" oninput="this.value = this.value.replace(/[^a-z0-9]/, '')" name="username" value="<?php echo $username; ?>">
		<small class="form-text text-muted">Der Benutzername kann später geändert werden.</small>
	</div>
	<div class="form-group">
		<label>E-Mail Adresse</label>
		<input type="email" class="form-control" placeholder="" oninput="this.value = this.value.replace(/[^a-zA-Z0-9@._]/, '')" name="email" value="<?php echo $email; ?>">
		<small class="form-text text-muted">Die E-Mail Adresse wird für andere Personen nicht ersichtlich sein.</small>
	</div> <!-- form-group end.// -->
	<div class="form-group">
		<label>Telefonnummer</label>
		<input type="number" class="form-control" placeholder="" oninput="this.value = this.value.replace(/[^0-9]/, '')" name="telnumber" value="<?php echo $telnumber; ?>">
		<small class="form-text text-muted">Die Telefonnummer wird für andere Personen nicht ersichtlich sein.</small>
	</div> <!-- form-group end.// -->
	<!-- <div class="form-group">
		<label>Geburtsdatum</label>
		<input type="date" class="form-control" placeholder="" name="bdaydate">
		<small class="form-text text-muted">Das Geburtsdatum wird für andere Personen nicht ersichtlich sein. <a style="color:red;">Mind. Alter: 18 Jahre</a></small>
	</div> */ <!-- form-group end.// -->
	
	
	<label>Birthday: </label>
<select name="month">
<option value="0">Select Month</option>
	<?php
		for( $m = 1; $m <= 12; $m++ ) {
			$num = str_pad( $m, 2, 0, STR_PAD_LEFT );
			$month = date( 'F', mktime( 0, 0, 0, $m + 1, 0, 0, 0 ) );
//if the above code won't work, you may try this:
//$month =  date("F", mktime(0, 0, 0, $m, 1));
			?>
				<option value="<?php echo $num; ?>"><?php echo $month; ?></option>
			<?php
		}
	?>
	</select>
	<select name="day">
	<option value="0">Select Day</option>
	<?php
		for( $a = 1; $a <= 31; $a++ ) {
		?>
				<option value="<?php echo $a; ?>"><?php echo $a; ?></option>
			<?php
		}
	?>
	</select>
	<select name="year">
	<option value="0">Select Year</option>
	<?php
	for( $y = 1990; $y <= 2100; $y++ ) {
			?>
				<option value="<?php echo $y; ?>"><?php echo $y; ?></option>
			<?php
		}
	?>
	</select>


	
		
		<div class="form-row">
		<div class="form-group col-md-6">
		  <label>Geschlecht</label>
		  <select id="inputState" class="form-control" name="gender">
			<option selected="">männlich</option>
		      <option>weiblich</option>
		      <option>sachlich</option>
		  </select>
		</div> <!-- form-group end.// -->
	</div> <!-- form-row.// -->
	
	<div class="form-row">
		<div class="form-group col-md-6">
		  <label>Wohnort</label>
		  <input type="text" class="form-control" oninput="this.value = this.value.replace(/[^a-zA-Z]/, '')" name="city">
		</div> <!-- form-group end.// -->
		<div class="form-group col-md-6">
		  <label>Land</label>
		  <select id="inputState" class="form-control" name="country">
			<option selected="">auswählen...</option>
		      <option>Afghanistan</option>
		      <option>Angola</option>
		      <option>Belgien</option>
		      <option>China</option>
			  <option>Deutschland</option>
			  <option>Frankreich</option>
			  <option>Honduras</option>
			  <option>Indien</option>
			  <option>Österreich</option>
			  <option>Pakistan</option>
			  <option>Russland</option>
			  <option>Schweiz</option>
			  <option>Uganda</option>
			  <option>Tunesien</option>
			  <option>Vereinigte Staaten von Amerika</option>
			  <option>Zypern</option>
		  </select>
		</div> <!-- form-group end.// -->
	</div> <!-- form-row.// -->
	<div class="form-group">
		<label>Passwort erstellen</label>
	    <input class="form-control" oninput="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')" type="password" name="password_1">
	</div>
	<div class="form-group">
		<label>Passwort wiederholen</label>
	    <input class="form-control" oninput="this.value = this.value.replace(/[^a-zA-Z0-9]/, '')" type="password" name="password_2">
	</div>
	
	<!-- form-group end.// -->  
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block" name="reg_user">Registrierung abschliessen</button> <!-- reg_user ist Name von Button, anschliessend werden Daten in DB geschrieben -->
    </div> <!-- form-group// -->      
    <small class="text-muted">Beim abschliessen der Registrierung akzeptieren sie unsere Lizenzvereinbarung und AGB's.</small>                                          
</form>
</article> <!-- card-body end .// -->
</div> <!-- card.// -->
</div> <!-- col.//-->

</div> <!-- row.//-->


</div> 
<!--container end.//-->

</br>



		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
	
	
<!-- Footer -->
<footer class="container-fluid text-center bg-lightgray">

        <div class="copyrights" style="margin-top:25px;">
            <p>GBCommunity © 2018, Alle Rechte vorbehalten
                <br>
                <span>Web Design By: Rogelio Barahona & Fabio Stecher</span></p>
            <p><a href="https://www.google.ch" target="_blank">Kontakt</a></p>
        </div>
</footer>
<!-- --------------------------------------------------->
</html>